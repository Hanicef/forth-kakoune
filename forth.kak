hook global BufCreate .*\.(fs|ft) %{
	set-option buffer filetype forth
}

hook global WinSetOption filetype=forth %{
	hook -once -always window WinSetOption filetype=.* %{ remove-hooks window forth-.+ }
}

hook -group forth-highlight global WinSetOption filetype=forth %{
	add-highlighter window/forth ref forth
	hook -once -always window WinSetOption filetype=.* %{ remove-highlighter window/forth }
}

add-highlighter shared/forth regions

add-highlighter shared/forth/string region '(?:^|\s)[cCsS\.]"\s' '"' fill value
add-highlighter shared/forth/displayed-string region '(?:^|\s)\.\(\s' \) fill value
add-highlighter shared/forth/escaped-string region '[sS]\\"\s' '"' fill value

add-highlighter shared/forth/comment region '(?:^|\s)\(\s' '\)' fill comment
add-highlighter shared/forth/line-comment region '(?:^|\s)\\\s' '$' fill comment
add-highlighter shared/forth/locals region '(?:^|\s)\{\s' \} fill attribute
add-highlighter shared/forth/locals-alt region '(?:^|\s)\{:\s' :\} fill attribute

add-highlighter shared/forth/number-conversion region '(?:^|\s)<#\s' '\s#>(?=\s)' group
add-highlighter shared/forth/number-conversion/ regex (?i)(?:^|\s)(<#|#>|#|#s|hold|sign|holds|xhold)(?=\s) 0:builtin
add-highlighter shared/forth/number-conversion/ ref forth/words

add-highlighter shared/forth/words default-region group
add-highlighter shared/forth/words/ regex (?:^|\s)(\$-?[0-9a-fA-F]+|%-?[01]+|#?-?[0-9]+|"'"."'")(?=\s) 0:value
add-highlighter shared/forth/words/ regex (?:^|\s)([\-\+]?[0-9]+(?:\.[0-9]*)?[Ee][\-\+]?[0-9]*)(?=\s) 0:value
add-highlighter shared/forth/words/ regex (?:^|\s):\s+[^\s]+(?=\s) 0:type
add-highlighter shared/forth/words/ regex (?i)(?:^|\s):noname(?=\s) 0:type
add-highlighter shared/forth/words/ regex (?:^|\s)\;(?=\s) 0:type

evaluate-commands %sh{
	q='"'
	t="'"

	values="bl false pad source true k-alt-mask k-ctrl-mask
	        k-delete k-down k-end k-f1 k-f2 k-f3 k-f4 k-f5 k-f6 k-f7 k-f8 k-f9
	        k-f10 k-f11 k-f12 k-home k-insert k-left k-next k-prior k-right
	        k-shift-mask k-up r/o r/w w/o"

	keywords="abort abort'$q' again align aligned begin allot buffer: \[char\]
	          char \[compile\] \["'"'$t'"'"\] case compile, constant create do
	          does> else endcase endof exit i if j
	          leave literal loop of postpone \+loop recurse repeat then quit
	          unloop until variable value while \?do 2constant 2literal 2value
	          2variable catch throw immediate evaluate execute begin-structure
	          cfield: end-structure field: \+field unused include-file include
	          includes require required dfalign dfaligned dffield: falign
	          faligned fconstant ffield: fliteral fvalue fvariable sfalign
	          sfaligned sffield: \(local\) locals\| ahead assembler bye
	          \[defined\] \[else\] \[if\] \[then\] \[undefined\] code cs-pick
	          cs-roll editor forget synonym \;code sliteral"

	functions="abs and cell\+ cells char\+ chars count drop dup
	           / /mod environment\? = fill invert lshift max min mod move
	           m\* - negate nip or over 1- 1\+ pick \+ roll rot rshift
	           sm/rem swap s>d tuck \* \*/ \*/mod 2drop 2dup 2/ 2over
	           2swap 2\* um/mod um* u< u> within word xor 0= 0< 0> 0<> < >
	           <> \?dup >body >number block buffer dabs d= dmax dmin d- dnegate
	           d\+ d2/ d2\* du< d0= d0< d< d>s m\+ m\*/ 2rot ekey>char ekey>fkey
	           accept action-of c, c@ cr c! , decimal defer defer@ defer! \.r
	           emit erase find @ hex 
	           is marker parse-name parse \+! refill restore-input r@
	           r> save-input space spaces ! to type "'"'$t'"'" 2@ 2r@ 2r>
	           2! 2>r u\.r u\. \. >r empty-buffers
	           flush list load save-buffers thru update d\.r d\. 
	           at-xy ms page bin close-file create-file
	           delete-file file-position file-size file-status flush-file
	           open-file read-file read-line
	           rename-file reposition-file resize-file
	           write-file write-line key\? time&date key source-id state
	           >in base depth scr ekey ekey\? emit\? blk here df@ dfloat\+
	           dfloats df! d>f fabs facos facosh falog fasin fasinh fatan
	           fatanh fatan2 fcos fcosh fdepth fdrop fdup f/ fexp fexpm1 fe\.
	           f@ fln flnp1 float\+ floats flog floor fmax fmin f- f\+ frot
	           fround fsin fsincos fsinh fsqrt fswap fs\. f! ftan ftanh ftrunc
	           f\* f\*\* f0= f0< f\. f< f~ f>d f>s precision set-precision sf@
	           sfloat\+ sfloats sf! s>f >float allocate free resize dump \.s
	           name>compile name>interpret name>string nr> n>r see
	           traverse-wordlist words \? also definitions find forth-wordlist
	           forth get-current get-order only order previous search-wordlist
	           set-current set-order wordlist blank cmove cmove> compare
	           /string -trailing replaces search substitute unescape ekey>xchar
	           -trailing-garbage \+x/string x-size x-width xc-size xc-width
	           xc, xc@\+ xchar- xchar\+ xc!\+ xc!\+\? xemit xkey xkey\?
	           x/string-"

	echo "
		add-highlighter shared/forth/words/ regex (?i)(?:^|\s)($(echo "$values" | tr '\n' ' ' | sed 's/\s\+/\|/g'))(?=\s) 0:value
		add-highlighter shared/forth/words/ regex (?i)(?:^|\s)($(echo "$keywords" | tr '\n' ' ' | sed 's/\s\+/\|/g'))(?=\s) 0:keyword
		add-highlighter shared/forth/words/ regex (?i)(?:^|\s)($(echo "$functions" | tr '\n' ' ' | sed 's/\s\+/\|/g'))(?=\s) 0:builtin
	"
}
